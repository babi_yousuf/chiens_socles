$(document).ready(function () {
    $(".slick-carousel").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
    });
    // Dropdown.init();
    $('dt').on('click', function () {
        $(this).toggleClass('open');
        $(this).parent('section').toggleClass('expanded');
    });
});

